## Functions

<dl>
<dt><a href="#cardsStore">cardsStore()</a></dt>
<dd><p>General store which persists data accross page reloads.
Returns couple of function to manipulate or retrieve the data.</p>
</dd>
<dt><a href="#addCard">addCard(card)</a> ⇒ <code><a href="#card">Array.&lt;card&gt;</a></code></dt>
<dd><p>Adds toggled card to temporary array.
If the card is second in the array then the Array is evaluated and emptied.
The array contains maximum of two cards.</p>
</dd>
<dt><a href="#getCards">getCards()</a> ⇒ <code><a href="#card">Array.&lt;card&gt;</a></code></dt>
<dd><p>Retrieves temporary array</p>
</dd>
<dt><a href="#flushTempArr">flushTempArr()</a></dt>
<dd><p>called when &#39;tempArray&#39; contains two cards</p>
</dd>
<dt><a href="#addAttempt">addAttempt()</a> ⇒ <code>number</code></dt>
<dd><p>adds 1 to &#39;attempts&#39; variable and returns couter whenever two cards are toggled</p>
</dd>
<dt><a href="#getAttempts">getAttempts()</a> ⇒ <code>number</code></dt>
<dd><p>returns attempts count</p>
</dd>
<dt><a href="#getGuessedCards">getGuessedCards()</a> ⇒ <code>Array.&lt;Card&gt;</code></dt>
<dd><p>returns array of successfully guesed cards</p>
</dd>
<dt><a href="#addGuessedCard">addGuessedCard(card)</a> ⇒ <code><a href="#card">Array.&lt;card&gt;</a></code></dt>
<dd><p>if two cards match, are added to array one by one</p>
</dd>
<dt><a href="#startTimer">startTimer()</a></dt>
<dd><p>Starts the counter when first card is toggled</p>
</dd>
<dt><a href="#stopTimer">stopTimer()</a></dt>
<dd><p>Stops the counter when all the cards have been guessed</p>
</dd>
<dt><a href="#isTimerRunning">isTimerRunning()</a></dt>
<dd><p>checks whether counter is running</p>
</dd>
<dt><a href="#getCurrentDuration">getCurrentDuration()</a></dt>
<dd><p>returns number of seconds that passed from game start</p>
</dd>
<dt><a href="#adjustStars">adjustStars(attempts)</a></dt>
<dd><p>Removes &#39;checked&#39; class from last element which has &#39;star&#39; class
when user gets to certain number of attempts.</p>
</dd>
<dt><a href="#toggleCard">toggleCard(evt, card, store)</a></dt>
<dd><p>Called whenever a card is clicked</p>
</dd>
<dt><a href="#elementCreator">elementCreator(type, text, classes, evenListenerType, eventListenerCallback)</a> ⇒ <code>HTMLElement</code></dt>
<dd><p>Creates HTML element that is ready to be added to the DOM</p>
</dd>
<dt><a href="#evaluateCards">evaluateCards(cards, store)</a></dt>
<dd><p>The function is called when &#39;store.tempArr&#39; contains exactly two cards.
If cards are not the same the are turn back down.board
Otherwise the cards are kept visible.</p>
</dd>
<dt><a href="#shuffle">shuffle(content)</a> ⇒ <code>Array.&lt;string&gt;</code></dt>
<dd><p>Doubles and shuffles provided array</p>
</dd>
<dt><a href="#renderCardContent">renderCardContent(arr)</a></dt>
<dd><p>Application&#39;s main function. 
Takes provided array, doubles it, shuffles it and
renders it along with other components.</p>
</dd>
</dl>

## Typedefs

<dl>
<dt><a href="#card">card</a> : <code>HTMLElement</code></dt>
<dd></dd>
</dl>

<a name="cardsStore"></a>

## cardsStore()
General store which persists data accross page reloads.
Returns couple of function to manipulate or retrieve the data.

**Kind**: global function  
<a name="cardsStore..startCounter"></a>

### cardsStore~startCounter() ⇒ <code>void</code>
**Kind**: inner method of [<code>cardsStore</code>](#cardsStore)  
**Returns**: <code>void</code> - Sets 'timeout' variable which recursively calls 'startCounter' function
every second and increments 'timer' variable.  

|  |
|
| 

<a name="addCard"></a>

## addCard(card) ⇒ [<code>Array.&lt;card&gt;</code>](#card)
Adds toggled card to temporary array.
If the card is second in the array then the Array is evaluated and emptied.
The array contains maximum of two cards.

**Kind**: global function  
**Returns**: [<code>Array.&lt;card&gt;</code>](#card) - cards  

| Param | Type |
| --- | --- |
| card | [<code>card</code>](#card) | 

<a name="getCards"></a>

## getCards() ⇒ [<code>Array.&lt;card&gt;</code>](#card)
Retrieves temporary array

**Kind**: global function  
**Returns**: [<code>Array.&lt;card&gt;</code>](#card) - cards  
<a name="flushTempArr"></a>

## flushTempArr()
called when 'tempArray' contains two cards

**Kind**: global function  
<a name="addAttempt"></a>

## addAttempt() ⇒ <code>number</code>
adds 1 to 'attempts' variable and returns couter whenever two cards are toggled

**Kind**: global function  
<a name="getAttempts"></a>

## getAttempts() ⇒ <code>number</code>
returns attempts count

**Kind**: global function  
<a name="getGuessedCards"></a>

## getGuessedCards() ⇒ <code>Array.&lt;Card&gt;</code>
returns array of successfully guesed cards

**Kind**: global function  
<a name="addGuessedCard"></a>

## addGuessedCard(card) ⇒ [<code>Array.&lt;card&gt;</code>](#card)
if two cards match, are added to array one by one

**Kind**: global function  
**Returns**: [<code>Array.&lt;card&gt;</code>](#card) - An array with already guessed cards  

| Param | Type | Description |
| --- | --- | --- |
| card | [<code>card</code>](#card) | A card to be added to array |

<a name="startTimer"></a>

## startTimer()
Starts the counter when first card is toggled

**Kind**: global function  
<a name="stopTimer"></a>

## stopTimer()
Stops the counter when all the cards have been guessed

**Kind**: global function  
<a name="isTimerRunning"></a>

## isTimerRunning()
checks whether counter is running

**Kind**: global function  
<a name="getCurrentDuration"></a>

## getCurrentDuration()
returns number of seconds that passed from game start

**Kind**: global function  
<a name="adjustStars"></a>

## adjustStars(attempts)
Removes 'checked' class from last element which has 'star' class
when user gets to certain number of attempts.

**Kind**: global function  

| Param | Type |
| --- | --- |
| attempts | <code>number</code> | 

<a name="toggleCard"></a>

## toggleCard(evt, card, store)
Called whenever a card is clicked

**Kind**: global function  

| Param | Type |
| --- | --- |
| evt | <code>event</code> | 
| card | <code>object</code> | 
| store | <code>function</code> | 

<a name="elementCreator"></a>

## elementCreator(type, text, classes, evenListenerType, eventListenerCallback) ⇒ <code>HTMLElement</code>
Creates HTML element that is ready to be added to the DOM

**Kind**: global function  

| Param | Type | Description |
| --- | --- | --- |
| type | <code>string</code> | Element TAG |
| text | <code>string</code> | Text to be displayed between the element's tags |
| classes | <code>Array.&lt;string&gt;</code> | Array of classes to be added to the element |
| evenListenerType | <code>string</code> | triggers event eventListenerCallback |
| eventListenerCallback | <code>function</code> | function to be called whenever 'evenListenerType' is triggererd |

**Example**  
```js
elementCreator('button', 'Click Me', ['styled-button'], 'click', onClick())
```
<a name="evaluateCards"></a>

## evaluateCards(cards, store)
The function is called when 'store.tempArr' contains exactly two cards.
If cards are not the same the are turn back down.board
Otherwise the cards are kept visible.

**Kind**: global function  

| Param | Type |
| --- | --- |
| cards | [<code>Array.&lt;card&gt;</code>](#card) | 
| store | <code>function</code> | 

<a name="shuffle"></a>

## shuffle(content) ⇒ <code>Array.&lt;string&gt;</code>
Doubles and shuffles provided array

**Kind**: global function  

| Param | Type | Description |
| --- | --- | --- |
| content | <code>Array.&lt;string&gt;</code> | Array of symbol visible on caards |

<a name="renderCardContent"></a>

## renderCardContent(arr)
Application's main function. 
Takes provided array, doubles it, shuffles it and
renders it along with other components.

**Kind**: global function  

| Param | Type |
| --- | --- |
| arr | <code>Array.&lt;string&gt;</code> | 

<a name="card"></a>

## card : <code>HTMLElement</code>
**Kind**: global typedef  
