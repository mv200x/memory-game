const board = document.querySelector('.board');
const fragment = document.createDocumentFragment();
const infoPanel = document.querySelector('.info-panel');
const symbolsArr = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H'];
const body = document.querySelector('body');

/**
 * @typedef {HTMLElement} card
 */

/**
 * General store which persists data accross page reloads.
 * Returns couple of function to manipulate or retrieve the data.
*/
const cardsStore = () => {
  let tempArr = [];
  let guessedCards = [];
  let attempts = 0;
  let timer = 0;
  let timerStarted = false;
  let timeOut;

  /**
   * @param {}
   * @returns {void}
   * Sets 'timeout' variable which recursively calls 'startCounter' function
   * every second and increments 'timer' variable.
   */
  const startCounter = () => {
    timerStarted = true;
    timeOut = setTimeout(() => {
      timer++;
      startCounter();
    }, 1000);
    const counter = document.querySelector('.counter');
    counter.textContent = `Duration: ${timer}`;
  };
  return {

    /** 
     * Adds toggled card to temporary array.
     * If the card is second in the array then the Array is evaluated and emptied.
     * The array contains maximum of two cards.
     * @param {card} card
     * @returns {card[]} cards
     */
    addCard: card => tempArr = [...tempArr, card],

    /** 
     * Retrieves temporary array 
     * @returns {card[]} cards
     */
    getCards: () => tempArr,

    /** 
     * called when 'tempArray' contains two cards 
     */
    flushTempArr: () => tempArr = [],

    /** 
     * adds 1 to 'attempts' variable and returns couter whenever two cards are toggled
     * @returns {number}
     */
    addAttempt: () => attempts += 1,

    /** 
     * returns attempts count 
     * @returns {number}
    */
    getAttempts: () => attempts,

    /** returns array of successfully guesed cards 
     * @returns {Card[]}
     */
    getGuessedCards: () => guessedCards,

    /** 
     * if two cards match, are added to array one by one
     * @param {card} card - A card to be added to array
     * @returns {card[]} An array with already guessed cards
     */
    addGuessedCard: card => guessedCards = [...guessedCards, card],

    /** Starts the counter when first card is toggled*/
    startTimer: () => startCounter(),

    /** Stops the counter when all the cards have been guessed*/
    stopTimer: () => clearTimeout(timeOut),

    /** checks whether counter is running  */
    isTimerRunning: () => timerStarted,

    /** returns number of seconds that passed from game start */
    getCurrentDuration: () => timer,
  }
};

/**
 * Removes 'checked' class from last element which has 'star' class
 * when user gets to certain number of attempts.
 * @param {number} attempts 
 */
const adjustStars = attempts => {
  if (attempts === 15 ||
    attempts === 23 ||
    attempts === 31 ||
    attempts === 39 ||
    attempts === 47
    ) {
    const stars = document.getElementsByClassName("checked");
    const i = stars.length;
    stars[i -1].classList.remove('checked');
  }
}

/**
 * Called whenever a card is clicked
 * @param {event} evt 
 * @param {object} card 
 * @param {function} store 
 */
const toggleCard = (evt, card, store) => {
  if (!store.isTimerRunning()) { // sets start time when first card is toggled
    store.startTimer();
  }
  const guessedCards = store.getGuessedCards();
  if (guessedCards.includes(card)) { // checks if already guessed card is not clicked again
    return;
  };
  const cardsInStore = store.getCards();
  card.classList.add('shown');
  card.firstChild.classList.add('shown');
  const cardObj = {card, value: evt.target.innerText};
  if (cardsInStore.length === 0) {
    store.addCard(cardObj);
    return;
  }
  if (cardsInStore.length === 1) {
    store.addAttempt();
    cardsInStore.map(c => {
      if (c.card.id !== cardObj.card.id) {  // checks if the clicked card is not already in store
        store.addCard(cardObj);
        evaluateCards(store.getCards(), store);
        const attemptsCount = store.getAttempts();
        adjustStars(attemptsCount);
        const attempts = document.getElementById('attempts');
        attempts.textContent = `Attempts: ${attemptsCount}`;
        store.flushTempArr();
      }
    })
    return;
  }
};


/**
 * Creates HTML element that is ready to be added to the DOM
 * 
 * @example
 *  elementCreator('button', 'Click Me', ['styled-button'], 'click', onClick())
 * @param {string} type Element TAG
 * @param {string} text Text to be displayed between the element's tags
 * @param {string[]} classes Array of classes to be added to the element
 * @param {string} evenListenerType triggers event eventListenerCallback
 * @param {function} eventListenerCallback function to be called whenever 'evenListenerType' is triggererd
 * @returns {HTMLElement}
 */
const elementCreator = (type, text, classes, evenListenerType, eventListenerCallback) => {
  const element = document.createElement(type);
  element.innerText = text;
  classes.map(c => {
    return element.classList.add(c);
  });
  element.addEventListener(evenListenerType, eventListenerCallback);
  return element;
}


/**
 * The function is called when 'store.tempArr' contains exactly two cards.
 * If cards are not the same the are turn back down.board
 * Otherwise the cards are kept visible.
 * @param {card[]} cards 
 * @param {function} store
 */
const evaluateCards = (cards, store) => {
  const {addGuessedCard, getGuessedCards, getAttempts, getCurrentDuration, stopTimer} = store;
  return cards.reduce((c1, c2) => {
    if (c1.value === c2.value) { // if 2 cards in tempArr ARE the same
      addGuessedCard(c1.card);
      addGuessedCard(c2.card);
      c1.card.classList.add('success');
      c2.card.classList.add('success');
      if (getGuessedCards().length === symbolsArr.length * 2) { // if all the cards have been guessed
        stopTimer();
        const winnerModal =  elementCreator('div', '', ['winner-modal']);
        const winnerContent = elementCreator('div', '', ['winner-content']);
        const refreshButton = elementCreator('button', 'New Game', ['refresh-button'], 'click', () => location.reload());
        const cancelButton = elementCreator('button', 'X', ['cancel-button'], 'click', () => winnerModal.remove());
        const stars = document.getElementsByClassName('fa fa-star checked');
        winnerContent.innerHTML = `
          <h1>GAME OVER</h1>
          <p>On ${getAttempts()} attempts</p>
          <p>Took you ${getCurrentDuration()} seconds</p>
          <p>Rating: ${stars.length}/5 stars<p/>
        `;
        winnerContent.appendChild(cancelButton);
        winnerContent.appendChild(refreshButton);
        winnerModal.appendChild(winnerContent);
        setTimeout(() => {
          body.appendChild(winnerModal);
        }, 1000);
      }
      return;
    } else { // if 2 cards in tempArr ARE NOT the same
      c1.card.classList.add('error');
      c2.card.classList.add('error');
      setTimeout(() => {
        c1.card.firstChild.classList.remove('shown');
        c1.card.classList.remove('shown');
        c1.card.classList.remove('error');
        c2.card.firstChild.classList.remove('shown');
        c2.card.classList.remove('shown');
        c2.card.classList.remove('error');
        return;
      }, 1000);
    }
  });
}

/**
 * Doubles and shuffles provided array
 * @param {string[]} content - Array of symbol visible on caards
 * @returns {string[]}
 */
const shuffle = content => {
  const doubled = content.reduce((acc, item) => {
    return [ ...acc,
     item,
     item,]
   }, []);

   let currentIndex = doubled.length, temporaryValue, randomIndex;

  while (currentIndex !== 0) {
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex -= 1;
      temporaryValue = doubled[currentIndex];
      doubled[currentIndex] = doubled[randomIndex];
      doubled[randomIndex] = temporaryValue;
  }
  return doubled;
}

/**
 * Application's main function. 
 * Takes provided array, doubles it, shuffles it and
 * renders it along with other components.
 * @param {string[]} arr 
 */
const renderCardContent = arr => {
  const shuffledArr = shuffle(arr);
  const store = cardsStore();
  
  shuffledArr.map((item, i) => {
    const card = elementCreator('div', '', ['card'], 'click', evt => toggleCard(evt, card, store));
    const cardContent = elementCreator('div', item, ['card-content']);
    card.setAttribute('id', `${i}`);
    card.appendChild(cardContent);
    fragment.appendChild(card);
  });
  const attempts = elementCreator('div', 'Attempts: 0', []);
  const refreshButton = elementCreator('button', 'New Game', ['refresh-button'], 'click', () => location.reload());
  const counter = elementCreator('div', 'Duration: 0', ['counter']);
  attempts.setAttribute('id', 'attempts');
  infoPanel.appendChild(refreshButton);
  infoPanel.appendChild(attempts);
  infoPanel.appendChild(counter);
  for (let i = 0; i < 5; i++) { // creates 5 stars
    const star = elementCreator('span', '', ['fa', 'fa-star', 'fa-lg', 'checked']);
    infoPanel.appendChild(star);
  }
  board.appendChild(fragment);
};

renderCardContent(symbolsArr);
